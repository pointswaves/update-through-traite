use std::fmt::Debug;
use std::str;

pub trait View: Debug {
    fn update(&mut self, indexs: &[usize], new: &[u8]) -> Result<(), &'static str>;
}

#[derive(Debug)]
struct Numbers {
    values: Vec<f64>,
}

impl View for Numbers {
    fn update(&mut self, indexs: &[usize], new: &[u8]) -> Result<(), &'static str> {
        let count: usize = new.len() / 8;
        if count != indexs.len() {
            Err("Bang")
        } else {
            for fdex in 0..count {
                let oct: [u8; 8] = [
                    new[fdex],
                    new[fdex + 1],
                    new[fdex + 2],
                    new[fdex + 3],
                    new[fdex + 4],
                    new[fdex + 5],
                    new[fdex + 6],
                    new[fdex + 7],
                ];
                let value = f64::from_be_bytes(oct);
                self.values[indexs[fdex]] = value;
            }
            Ok(())
        }
    }
}

#[derive(Debug)]
struct Words {
    values: Vec<String>,
}

impl View for Words {
    fn update(&mut self, indexs: &[usize], new: &[u8]) -> Result<(), &'static str> {
        self.values[indexs[0]] = String::from(str::from_utf8(&new).unwrap());
        Ok(())
    }
}

fn main() {

    // I want a trait that can do things like render and be conpaired
    // see https://gitlab.com/pointswaves/dotted-lines/-/blob/master/src/view/mod.rs#L57
    // but i would also like to be able to update the object

    // I want to accept and store the graphs as Vec<Box<dyn View>> so that
    // others can supply there own

    // I want to be able to plot arbitary data, for now i have
    // `fn update_trace(&mut self, index: usize, data: Vec<f64>) -> Result<(), &'static str>;`
    // Which is fairly flex as the underlying data could be (f64), (f64, f64) (f32, f32, f32)
    // but is not so helpful for catigory data that might be a (string) or Enum

    // u8 seems like the most common thing for things to be `cast` to or from

    // alternatively page could be defined for a data Type

    // Then you could update all of the data in the Type
    // and use a `Lens` https://github.com/xi-editor/druid/blob/master/druid/src/lens/lens.rs#L211
    // like thing so that all the views were stored in Vec<Box<dyn View<T>>>
    // were dyn View<T> was a lens that focused the data to the raw dyn View<U>  that others could implements
    // this would not need to be as complex as druids lens as the views would only react to change so would not need to
    // be able to propergate data back.

    let mut boxes: Vec<Box<dyn View>> = vec![
        Box::new(Numbers {
            values: vec![1.0, 2.0],
        }),
        Box::new(Words {
            values: vec![String::from("bob")],
        }),
    ];

    boxes[0].update(&[0], &f64::to_be_bytes(5.6)).unwrap();
    boxes[1].update(&[0], &[100, 101, 102, 103]).unwrap();

    println!("{:?}", boxes)
}
